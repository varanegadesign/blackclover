<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Libs\Login;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    // productos
    public function productsElement($length = 2){

     $array	=   [];

     for($i=0; $i < $length; $i++){
         $id = 'product-'.($i + 1);
         $array[]	=   $this->campaignElement($id);
     }

     return $array;
    }

    public function productElement(){

     return [
           'id' => $id,
         ];
    }

    public function admin() {
      return response()->json([
          'campaigns'	 =>	$this->productsElement(8),
      ]);
    }
}
