<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Black Clover | Admin</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.0.46/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="{{ mix('/css/fonts.css') }}">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
      <div class="container" id="app">
        <admin-panel></admin-panel>
      </div>
      <script src="{{ mix('/js/app.js') }}" charset="utf-8"></script>
    </body>
</html>
