<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::latest()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $product = new Product();
         $product->ref = $request->ref;
         $product->name = $request->name;
         $product->category = $request->category;
         $product->type = $request->type;
         $product->date = $request->date;
         $product->stock = $request->stock;
         $product->sizes = $request->sizes;
         $product->prices = $request->prices;
         $product->avariable = $request->avariable;
         $product->save();
         return $product;
         // return redirect('/api/tasks');
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         $product = Product::findOrFail($id);
         $product->ref = $request->ref;
         $product->category = $request->category;
         $product->type = $request->type;
         $product->date = $request->date;
         $product->stock = $request->stock;
         $product->sizes = $request->sizes;
         $product->prices = $request->prices;
         $product->avariable = $request->avariable;
         $product->update();

         return $product;
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         $product = Product::findOrFail($id);
         $product->delete();

         return 204;
     }
 }
